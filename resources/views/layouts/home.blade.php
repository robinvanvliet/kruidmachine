<!DOCTYPE html>
<meta lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type='text/css' href="{{ URL::asset('css/main.css') }}"/>
	<title>Spice </title>
</head>
	<body>
		<main>
			<div id="container_main">

			<a class="user_container" href="{{ url('/login')}}" id="user_container" style="cursor: pointer;">
				
				<span class="user_name" id="user_name" >Login/<br>Register</span>
			</a>

			 <nav id="layout_nav">
				<!-- <a id ="logo" href=""><img src="../../../img/logoBlack.png" title="source: imgur.com" /></a> -->
				<img class="login-logo-image" src={{url::asset('/img/Logo.svg')}}  alt="logo">
				<ul>
					<li>
						<a class="active" href="{{ url('/')}}">Home</a>
						<div class="nav__line" style="opacity: 0;"></div>
					</li>

					<li>
						<a href="{{url ('/kruid') }}">Spices</a>
						<div class="nav__line"></div>
					</li>

					<li>
						<a href="{{ url('/mix')}}">Mixes</a>
						<div class="nav__line"></div>
					</li>
					</ul>
				</nav>

				<footer>
					copyright R&B 
				</footer>

				<div id="container_content">
					@yield('content')
				</div>
				
		</main>
	</body>
</meta>
