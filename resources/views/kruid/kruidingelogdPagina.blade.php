@extends('layouts.kruidingelogd')
@section('content')
	<!-- Javascript om de create/edit modals en de achtergrond escape asset weer te geven/sluiten -->
	<script>
		function newspice_show(){
			document.getElementById("background_create_exit_asset").style.display = "block";
			document.getElementById("kruid_new_container").style.display = "block";
		}
		function newspice_hide() {
			document.getElementById("kruid_new_container").style.display = "none";
			document.getElementById("background_create_exit_asset").style.display = "none";
		}
		function editspice_hide(i){
			if(i == 'ALL'){
				for(let a = 0; a < document.getElementsByClassName("kruid_edit_container").length; a++){
					document.getElementsByClassName("kruid_edit_container")[a].style.display = "none";
				}
			}
			else{
				document.getElementsByClassName("kruid_edit_container")[i].style.display = "none";
			}
			document.getElementById("background_edit_exit_asset").style.display = "none";
		}

		function editspice_show(i){
			document.getElementById("background_edit_exit_asset").style.display = "block";
			document.getElementsByClassName("kruid_edit_container")[i].style.display = "block";
		}
	</script>
	<!---->


	
	<!-- Modal om een kruid toe te voegen -->
	<div id="kruid_new_container">
		<h1 id="kruid_editform_title">Add a spice</h1>
		<span id="kruid_editform_closeicon" onclick="newspice_hide()">x</span>
		<form id="kruid_editform" action="/kruid/nieuw" enctype="multipart/form-data" method="POST">
			{{ csrf_field() }}
			<label class="kruid_editform_labels"  for="kruid">Name of spice: </label>
			<input class="kruid_editform_inputfield"  type="text" name="kruid" value="">
			<label class="kruid_editform_labels" for="image">Foto: </label>
			<input class="kruid_editform_inputfield" type="file" name="image" value="">
			<button id = "kruid_editform_postbutton" type="submit" name="button">Add</button>
		</form>
	</div>
	<!---->


	<!-- Bewaar in $arrayComp de kruiden die aanwezig zijn in de compartimenten -->
	@php($arrayComp = array())
	@foreach($compartiment as $compartiment)
		@php($arrayComp[] = $compartiment->comp_kruid)
	@endforeach
	<!---->



	<!-- Bewaar in $arrayMix alle kruiden die aanwezig zijn in de recepten van de ingelogde gebruiker-->
	@php($arrayMix = array())
	@foreach($mix as $mixes)
		@if (in_array($mixes->kruid1, $arrayMix) === FALSE and $mixes->gebruikersnaam === Auth::user()->name)
			@php($arrayMix[] = $mixes->kruid1)
		@endif

		@if (in_array($mixes->kruid2, $arrayMix) === FALSE and $mixes->gebruikersnaam === Auth::user()->name)
			@php($arrayMix[] = $mixes->kruid2)
		@endif

		@if (in_array($mixes->kruid3, $arrayMix) === FALSE and $mixes->gebruikersnaam === Auth::user()->name)
			@php($arrayMix[] = $mixes->kruid3)
		@endif
	@endforeach
	<!---->


	<!-- Maak edit modals voor elk kruid -->
	@php($count=0)
	@foreach($kruid as $kruid)
		<div class ="kruid_edit_container">
			<h1 id="kruid_editform_title">Editing {{$kruid->kruid}}</h1>
			<span id="kruid_editform_closeicon" onclick="editspice_hide({{$count}})">x</span>
			
			<form id="kruid_editform" action="{{action('KruidController@update', $kruid['kruid'])}}" enctype="multipart/form-data" method="POST">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<label class="kruid_editform_labels" for="kruid">Name of spice: </label>
				<input class="kruid_editform_inputfield" type="text" name="kruid" value="{{$kruid->kruid}}">
				<label class="kruid_editform_labels" for="image">Foto: </label>
				<input class="kruid_editform_inputfield" type="file" name="image" value="">
				@if (in_array($kruid->kruid, $arrayComp) and in_array($kruid->kruid, $arrayMix))
					<p class="kruid_editform_warning">This spice is currently in a mix and a compartment. <br>To edit please remove it from the mix recept and compartment.</p>
					<div style="text-align: center; background-color: gray;padding-top: 7.5px;"id = "kruid_editform_postbutton" style="background-color: gray;">Update</div>
				@elseif(in_array($kruid->kruid, $arrayMix))
					<p class="kruid_editform_warning">This spice is currently in a mix. In order to edit please remove it from the mix recept.</p>
					<div style="text-align: center; background-color: gray;padding-top: 7.5px;"id = "kruid_editform_postbutton" style="background-color: gray;">Update</div>
				@elseif(in_array($kruid->kruid, $arrayComp))
					<p class="kruid_editform_warning">This spice is currently in a compartment. In order to edit please remove it from the compartment.</p>
					<div style="text-align: center; background-color: gray;padding-top: 7.5px;"id = "kruid_editform_postbutton" style="background-color: gray;">Update</div>
					
					@else
					<button id = "kruid_editform_postbutton" type="submit" name="button">Update</button>
				@endif
			</form>
		</div>
		@php($count++)
	@endforeach
	<!---->
	

	<div id = "kruid_titel_container">
		<h1 id="kruid_titel">Spices</h1>
	</div>
	
	<div id="kruid_container">
		<div class='gridcontainer'>
			<!-- Griditem om een kruid toe te voegen -->
			<a class="gridlink" onclick="newspice_show()" style="cursor: pointer;">
				<div class='griditem add-mix-card'>
					<img class='item_image_new' src='{{ asset("img/plusicon.png") }}' >
					<p class='item_description_new'>Add a spice</p>
				</div>
			</a>
			<!---->

			<!-- Loop door alle kruiden en maak per kruid in een griditem --->
			@php($counto=0)
			@foreach($kruiden as $kruid)
				<div class="gridlink">
					<div class='griditem'>
						<img class='item_image' src='/img/{{$kruid->img_path}}'>
						<p class='item_description'>{{$kruid->kruid}}</p>
						<div class='item_icons_container'>
							<a onclick="editspice_show({{$counto}})"><img id="image_edit"src="{{ asset("img/edit.png") }}"></img></a>
							<!-- Als een kruid in een mix recept of in de compartiment zit is het niet mogelijk om deze aan te passen of verwijderen --->
							<form id="kruid_delete_option" action="{{action('KruidController@destroy', $kruid['kruid'])}}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<!-- Vergelijkingen met de vorige gemaakte mix en compartiment kruid arrays --->
								@if (in_array($kruid->kruid, $arrayComp) and in_array($kruid->kruid, $arrayMix)) 
										<div id="image_delete" onclick="return confirm('Please remove this spice from your mix recept and compartment first.')" type="submit" name="button" style="background-image: url({{ asset("img/trash.png") }}")></div>
									@elseif(in_array($kruid->kruid, $arrayMix))
										<div id="image_delete" onclick="return confirm('Please remove this spice from your mix recept first.')" type="submit" name="button" style="background-image: url({{ asset("img/trash.png") }}")></div>
									
									@elseif(in_array($kruid->kruid, $arrayComp))
										<div id="image_delete" onclick="return confirm('Please remove this spice from your compartment first.')" type="submit" name="button" style="background-image: url({{ asset("img/trash.png") }}")></div>
									@else
										<button id="image_delete" onclick="return confirm('Are you sure?')" type="submit" name="button" style="background-image: url({{ asset("img/trash.png") }}")></button>
								@endif
							</form>
						</div>
					</div>
				</div>
			@php($counto++)
			@endforeach
			<!---->
		</div>
	</div>

	<!-- Achtergrond escape asset --->
	<div id="background_create_exit_asset" onclick="newspice_hide()"></div>
	<div id="background_edit_exit_asset" onclick="editspice_hide('ALL')"></div>
	<!---->
	
@endsection
