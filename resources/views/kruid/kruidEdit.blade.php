@extends('layouts.kruidingelogd')
@section('content')
<div id="kruid_nieuw_container" style="display: block; border: 0;">
	<h1 id="kruid_editform_title">Editing {{$kruid->kruid}}</h1>
	<form id="kruid_editform" action="{{action('KruidController@update', $kruid['kruid'])}}" enctype="multipart/form-data" method="POST">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<label class="kruid_editform_labels" for="kruid">Name of spice: </label>
		<input class="kruid_editform_inputfield" type="text" name="kruid" value="{{$kruid->kruid}}">
		<label class="kruid_editform_labels" for="image">Foto: </label>
		<input class="kruid_editform_inputfield" type="file" name="image" value="">
		<button id = "kruid_editform_postbutton" type="submit" name="button">Update</button>
		<a id = "kruid_editform_backbutton" href="{{ url()->previous() }}">Back</a>
	</form>

</div>
@endsection
