<!DOCTYPE html>
<meta lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type='text/css' href="{{ URL::asset('css/main.css') }}"/>
	<title>Spice </title>
</head>
	<body>
		<main>
			<div id="container_main">
			 <nav id="layout_nav">
				<!-- <a id ="logo" href=""><img src="../../../img/logoBlack.png" title="source: imgur.com" /></a> -->
				<img class="login-logo-image" src={{url::asset('/img/Logo.svg')}} alt="logo">
				<ul>
					<li>
						<a href="{{ url('/')}}">Compartments</a>
						<div class="nav__line"></div>
					</li>

					<li>
						<a class="active" href="{{url ('/kruid') }}">spices</a>
						<div class="nav__line" style="opacity: 0;"></div>
					</li>

					<li>
						<a href="{{ url('/mix')}}">mixes</a>
						<div class="nav__line"></div>
					</li>

					<!--<li>
						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
						<div class="nav__line"></div>
					</li>-->
				</ul>
				</nav>

				<div id="container_content">
					@yield('content')
				</div>
		</main>
	</body>
</meta>
