@extends('layouts.home')
@section('content')

<div class="back-arrow">
    <a class="back-arrowA" href="{{ url('/')}}"><img class="arrow-image" src="../img/Arrow4.png" alt="">Return To Home</a>
</div>

<h1 class="updateCompTitel">Choose a new spice - Compartiment {{$compartiment->comp_nummer}}</h1>

<ol class="updateCompList">
    <li>
        Place a bowl on the platform within the machine.
    </li>
    <li>
        Fill the compartment with your desired spice.
    </li>
    <li>
        Click on one of the spices to add it to the current compartment. <i>(If no spices are visible, add them on the "spices" page)</i>
    </li>
</ol>
<div class="updateKruidContainer">
    <div class='gridcontainer'>
        @foreach($kruid as $kruid)
            <div class="gridlink">
                <form class="updateCompForm" method="post" action="/kruid/kruid_update_comp1/{{$compartiment->comp_nummer}}">
                    {{csrf_field()}}
                    @method('patch')
                        <?php /*<select class="compSelect compSpice" name="kruid">
            +                @foreach($kruid as $kruid)
            +                    <option value="{{$kruid->kruid}}">{{$kruid->kruid}}</option>
            +               @endforeach
                        </select> */ ?>
                        <?php /* <button type="submit" class="update_kruid update_kruid2">Update</button> */ ?>
                        
                        <input type="hidden" value="{{$kruid->kruid}}" name="kruid"></input>

                        <button class="updateCompButton" name="button" type="submit">
                                <div class='updateCompItem'>
                                    <p class='updateCompDescription'>{{$kruid->kruid}}</p>
                                    <img class='compImage' src='/img/{{$kruid->img_path}}'>
                            </div>
                        </button>
                    
                </form>
            </div>
        @endforeach
    </div>
</div>

@endsection
