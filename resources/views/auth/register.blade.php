@extends('layouts.login')

@section('content')
<div class="login-arrow">
    <a href="{{ url('/')}}"><img class="arrow-image" src="../img/Arrow4.png" alt="">Return To Home</a></div>
        <div class="login-container">
             <div class="row justify-content-center login-container-item-wrapper">
                 <div class="login-content">
            <!-- <div class="card">
                <div class="card-header">{{ __('Register') }}</div> -->

                <div class="card-body">
                <div class="login-logo">
                        <img class="login-logo-image" src='/img/Logo.svg' alt="logo">
                    </div>

                    <div class="login-header">{{ __('Register') }}</div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="login-label col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Name" id="name" type="text" class="login-input form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="login-label col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input placeholder="E-mail" id="email" type="email" class="login-input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="login-label col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                          <div class="password-message"> <p>{{ __('*The password must be at least 8 characters.') }}</p> </div>

                            <div class="col-md-6">
                              <input placeholder="Password" id="password" type="password" class="login-input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                  <br>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="login-label col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Confirm password" id="password-confirm" type="password" class="login-input form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="login-button btn btn_register">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
