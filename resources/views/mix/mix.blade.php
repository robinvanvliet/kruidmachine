@extends('mix.layoutingelogd')
@section('content')

<!-- hoi -->

	<div id = "kruid_titel_container">
		<h1 id="kruid_titel">Mixes</h1>
	</div>
		
	
	<div id="kruid_container">
	
	<div class='gridcontainer gridcontainer--mix'>
		<a class="gridlink" style="cursor: pointer;">
			<div class='griditem griditem--mix add-mix-card'>
				<img class='item_image_new' src='{{ asset("img/plusicon.png") }}'/>
				<p class='item_description_new item_description_new--mix'>Add a mix</p>
			</div>
		</a>


	@foreach ($mix as $mix)
	<div class="griditem griditem--mix">
	<a href="{{route ('editmix', $mix->naam)}}" class="mix-edit">edit</a>
		<div class="griditem-mix">
		<header>
			
		<h4 class="mix-title" ><div class="mix-circle"></div> {{$mix->naam}} </h4>
			
			
		</header>
		

		@php($arrayKruid = array())

		@foreach($kruid as $kruiden)	
			@php($arrayKruid[] = $kruiden->kruid)
		@endforeach

		<ul class="mix-spice-list"> 


		@if(in_array($mix->kruid1, $arrayKruid) === FALSE)
			<li class ="mix-li-items">{{$mix->kruid1}}</li>
			
		@else
			<li class ="mix-li-items">{{$mix->kruid1}}</li>
		@endif


		@if(in_array($mix->kruid2, $arrayKruid) === FALSE)
			<li class ="mix-li-items" >{{$mix->kruid2}}</li>
		@else
			<li class ="mix-li-items">{{$mix->kruid2}}</li>
			
		@endif
		

		@if(in_array($mix->kruid3, $arrayKruid) === FALSE)
			<li class= "mix-li-items">{{$mix->kruid3}}</li>
		@else
			<li class ="mix-li-items">{{$mix->kruid3}}</li>
		@endif
		
		</ul>

		<ul class="mix-amount-list">
			<li class ="mix-li-items">{{$mix->hoeveelheid1}} gr
				@if(in_array($mix->kruid1, $arrayKruid) === FALSE)
				<form action="{{action('KruidController@store')}}" method="POST">
						{{ csrf_field() }}
						{{method_field('POST')}}
						<input style="display: none;"class="kruid_editform_inputfield" type="text" name="kruid" value="{{$mix->kruid1}}">
						<button onclick="return confirm('This spice does not exist. Create spice: {{$mix->kruid1}}?')" class = "mix_warning_button" type="submit" name="button">!</button>
					</form>
				@endif
			</li>
			<li class ="mix-li-items">{{$mix->hoeveelheid2}} gr
				@if(in_array($mix->kruid2, $arrayKruid) === FALSE)
				<form action="{{action('KruidController@store')}}" method="POST">
						{{ csrf_field() }}
						{{method_field('POST')}}
						<input style="display: none;"class="kruid_editform_inputfield" type="text" name="kruid" value="{{$mix->kruid2}}">
						<button onclick="return confirm('This spice does not exist. Create spice: {{$mix->kruid2}}?')" class = "mix_warning_button" type="submit" name="button">!</button>
					</form>
				@endif
			</li>
			<li class ="mix-li-items">{{$mix->hoeveelheid3}} gr
				@if(in_array($mix->kruid3, $arrayKruid) === FALSE)
				<form action="{{action('KruidController@store')}}" method="POST">
						{{ csrf_field() }}
						{{method_field('POST')}}
						<input style="display: none;"class="kruid_editform_inputfield" type="text" name="kruid" value="{{$mix->kruid3}}">
						<button onclick="return confirm('This spice does not exist. Create spice: {{$mix->kruid3}}?')" class = "mix_warning_button" type="submit" name="button">!</button>
					</form>
				@endif
			</li>
		</ul>

		@if(in_array($mix->kruid1, $arrayKruid) and in_array($mix->kruid2, $arrayKruid) and in_array($mix->kruid3, $arrayKruid))
			<form action="/mix/update/{{$mix->naam}}" method="POST"> 
				{{ csrf_field() }}
				{{method_field('PATCH')}}
					<button type="submit" class="mix-dispense-button">Dispense</button>
			</form>
		@else
			<button type="submit" class="mix-dispense-button" style="background-color: gray;">Cannot Dispense</button>
		@endif
		
		</div>

	</div>
	@endforeach

	
		

</div>
</div>
</div>
{{-- Modal Add Recipe --}}
<div class="add-mix-modal">
	<div class="add-mix-modal-content">
		<span class ="iconify add-mix-modal-content-close-button" data-inline="false" data-icon="ant-design:close-outlined"></span>
		<div class="add-mix-modal-content-header" ><h2>Create New Recipe</h2></div>
		<div class="add-mix-modal-content-container">
			<form class="add-mix-modal-content-container-form" id="" action="/mix/nieuw" method="POST">
				{{ csrf_field() }}
				<!-- {{method_field('POST')}} -->
				
				{{-- Mix Naam --}}
				<label for="naam">Mix name:</label>
				<input class="add-mix-modal-content-form-input" type="text" name="naam" value="">

				{{-- Spice 1 --}}
				<label for="kruid1">Spice 1: </label>
				<select class="add-mix-modal-content-container-form-kruid-select" name=kruid1>
					@foreach($kruid as $kruiden)
					<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>
					@endforeach
				</select>

				<label for="Hoeveelheid1">Amount:</label>
				<input name="hoeveelheid1" type="number" min="0" max="50" value="">
				<select class="add-mix-modal-content-container-form-hoeveelheid-select" name="hoeveelheid1_type">
					<option title="teaspoon" value="tsp">tsp</option>
					<option value="gr">grams</option>
				</select>

					{{-- Spice 2 --}}
				<label for="kruid1">Spice 2: </label>
				<select class="add-mix-modal-content-container-form-kruid-select" name=kruid2>
					@foreach($kruid as $kruiden)
					<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>
					@endforeach
				</select>

				<label for="Hoeveelheid2">Amount:</label>
				<input name="hoeveelheid2" type="number" min="0" max="50" value="">
				<select class="add-mix-modal-content-container-form-hoeveelheid-select" name="hoeveelheid2_type">
					<option title="teaspoon"  value="tsp">tsp</option>
					<option value="gr">grams</option>
				</select>

					{{-- Spice 3 --}}
				<label for="kruid1">Spice 3: </label>
				<select class="add-mix-modal-content-container-form-kruid-select" name=kruid3>
					@foreach($kruid as $kruiden)
					<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>
					@endforeach
				</select>

				<label for="Hoeveelheid3">Amount:</label>
				<input name="hoeveelheid3" type="number" min="0" max="50" value="">
				<select class="add-mix-modal-content-container-form-hoeveelheid-select" name="hoeveelheid3_type">
					<option title="teaspoon"  value="tsp">tsp</option>
					<option value="gr">grams</option>
				</select>

				<button type="submit" name="button" class="add-mix-modal-content-container-form-submit">Add New Recipe</button>
		</div>
	</div>
</div>
</div>
@endsection
<!-- <a href="{{ url('/mix/nieuw') }}" class="btn_nieuw">nieuwe mix <div class="btn_plus">+</div></a> -->
