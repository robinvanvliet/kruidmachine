@extends('mix.layoutingelogd')
@section('content')
		@if (session('error'))
			<div class="error">{{ session('error') }}</div>
		@endif	
		<div class="mixEdit">
		<form class="mixEdit-form" action="/mix/edit/{{$mix->naam}}" method="POST">
		{{csrf_field()}}
		{{method_field('PATCH')}}
		<h1>{{$mix->naam}}</h1>
		<label for="edit">Edit Mix Name:<label>
		<input type="text" name="naam" value="{{$mix->naam}}">

		{{-- Spice 1 --}}
		<label for="kruid1">Spice 1:</label>
		<select name="kruid1" class="mix-edit-spice">
		<option selected >{{$mix->kruid1}}</option>
		@foreach($kruid1 as $kruiden)
				<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>			
		@endforeach
		</select>

		<label for="Hoeveelheid1">Amount Spice 1: </label>
		<input class="edit-hoeveelheid-input" type="number" min="0" max="50" name="hoeveelheid1" value="{{ $mixHoeveelheidNummer1 }}">
		<select class="mix-edit-hoeveelheid" name="hoeveelheid1_type">
			<option value="{{ $mixHoeveelheidType1 }}">{{ $mixHoeveelheidType1 }}</option>
			<option value="{{ $mixType1 }}">{{ $mixType1 }}</option>
		</select>

		{{-- Spice 2 --}}
			<label for="kruid2">Spice 2:</label>
		<select name="kruid2" class="mix-edit-spice">
		<option selected >{{$mix->kruid2}}</option>
		@foreach($kruid1 as $kruiden)
				<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>			
		@endforeach
		</select>

		<label for="Hoeveelheid2">Amount Spice 2: </label>
		<input class="edit-hoeveelheid-input" type="number" min="0" max="50" name="hoeveelheid2" value="{{ $mixHoeveelheidNummer2 }}">
		<select class="mix-edit-hoeveelheid" name="hoeveelheid2_type">
			<option value="{{ $mixHoeveelheidType2 }}">{{ $mixHoeveelheidType2 }}</option>
			<option value="{{ $mixType2 }}">{{ $mixType2 }}</option>
		</select>

		{{-- Spice 3 --}}

		<label for="kruid3">Spice 3:</label>
		<select name="kruid3" class="mix-edit-spice">
		<option selected>{{$mix->kruid3}}</option>
		@foreach($kruid1 as $kruiden) 
				<option value="{{$kruiden->kruid}}">{{$kruiden->kruid}}</option>			
		@endforeach
		</select>

		<label for="Hoeveelheid3">Amount Spice 3: </label>
		<input class="edit-hoeveelheid-input" type="number" min="0" max="50" name="hoeveelheid3" value="{{ $mixHoeveelheidNummer3 }}">
		<select class="mix-edit-hoeveelheid" name="hoeveelheid3_type">
			<option value="{{ $mixHoeveelheidType3 }}">{{ $mixHoeveelheidType3 }}</option>
			<option value="{{ $mixType3 }}">{{ $mixType3 }}</option>
		</select>
		<button id="update-button" type="submit" name="button" class="btn btn_update">Update</button>
	
		</form>
		<form class="form_delete"action="/mix/edit/{{$mix->naam}}" method="POST"  id="delete-mix">
		{{ csrf_field() }}
		<button type="submit" name="button" class="btn edit-btn-delete">Delete</button>

		{{ method_field('DELETE') }} 
		</form>
		</div>

@endsection
