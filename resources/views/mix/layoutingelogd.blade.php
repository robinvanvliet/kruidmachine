<!DOCTYPE html>
<meta lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type='text/css' href="{{ URL::asset('css/main.css') }}"/>
	<script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
	<script type="text/javascript" src="{{URL::asset("js/main.js")}}"></script>
	<title>Spice </title>
</head>
	<body>
		<main>
			<div id="container_main">

				<div id="user_container">
				
					<span id="user_name">{{ Auth::user()->name }}</span>
					<img src="{{ asset("img/drop-down-arrow.png") }}" id="dropdown_icon" onclick="dropdown_icon_show()"></img>

					<script>
						function dropdown_icon_show() {
					    document.getElementsByClassName("dropdown_items")[0].style.display = "block";
							document.getElementById("dropdown_icon").setAttribute("onclick","dropdown_icon_hide()");
						}
						function dropdown_icon_hide() {
					    document.getElementsByClassName("dropdown_items")[0].style.display = "none";
							document.getElementById("dropdown_icon").setAttribute("onclick","dropdown_icon_show()");
						}
					</script>

					<div class="dropdown_items" >
						<div class="dropdown_item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							<a id="logout-text">{{ __('Logout') }}</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
						</div>
					</div>
				</div>

			 <nav id="layout_nav">
			 	<img class="login-logo-image" src={{url::asset('/img/Logo.svg')}} alt="logo">
				<ul>
					<li>
						<a href="{{ url('/')}}">Home</a>
						<div class="nav__line"></div>
					</li>

					<li>
						<a href="{{url ('/kruid') }}">Spices</a>
						<div class="nav__line"></div>
					</li>

					<li>
						<a class="active" href="{{ url('/mix')}}">Mixes</a>
						<div class="nav__line" style="opacity: 0;"></div>
					</li>

				</ul>
				</nav>
				<footer>
					copyright R&B
				</footer>
				<div id="container_content">
					@yield('content')
				<div>	
			</div>
		</main>

	</body>
</meta>
