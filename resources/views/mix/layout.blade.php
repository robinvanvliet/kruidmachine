<!DOCTYPE html>
<meta lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type='text/css' href="{{ URL::asset('css/main.css') }}"/>
	<script type="text/javascript" src="{{URL::asset("js/main.js")}}"></script>
	<script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>

	<title>MIX</title>
</head>
	<body>
			<div class="filter-layer"></div>
		<header>
		<nav id="layout_nav">
			<a id ="logo" href="">
				<!-- <img src="{{URL::asset('img/logo.png') }}" alt=""> -->
				<img src="../../../img/logo2.png" title="logo" />
			</a>
			<ul>
				<li class="nav-li">
					<a href="{{url ('/') }}">home</a>
					<div class="nav__line"></div>
				</li>
				<div class="streepje">_</div>
				<li class="nav-li">
					<a href="{{url ('/kruid') }}">spices</a>
					<div class="nav__line"></div>
				</li>
				<div class="streepje">_</div>
				<li class="nav-li">
					<a class="active" href="{{ url('/mix')}}">mixes</a>
					<!-- <div class="nav__line"></div> -->
				</li>
				<div class="streepje">_</div>
				<li class="nav-li">
					<a href="{{ url('/login')}}">login/register</a>
					<div class="nav__line"></div>
				</li>
			</ul>
		</nav>
	</header>

		<main class="main-flexbox">
			<div id="container_main">
				@yield('content')
			</div>
		</main>
	</body>
</head>
