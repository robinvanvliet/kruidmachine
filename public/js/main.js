window.onload = function() {
    const modal = document.querySelector(".add-mix-modal");
    const trigger = document.querySelector(".add-mix-card");
    const closeButton = document.querySelector(".add-mix-modal-content-close-button");

    const toggleModal = () => {
        modal.classList.toggle("show-add-mix-modal");
    }


    const windowOnClick = (event) => {
        if (event.target === modal) {
            toggleModal();
        }
    }
    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
   

}