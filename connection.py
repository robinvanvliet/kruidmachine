import mysql.connector
import time

import serial
import os

import sys

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="kruidmachine"
)

mycursor = mydb.cursor()

port = serial.Serial('COM3', 9600)
port.timeout = 1

port2 = serial.Serial('COM4', 9600)
port2.timeout = 1


while True:

    mycursor.execute(
        "SELECT comp_nummer FROM compartiment WHERE comp_veranderen = 'ja';")

    for x in mycursor:

        comp = str(x[0])

        if (x[0] == 1):
            print("Vertuur compartiment: " + comp + " bericht")
            port.write("comp1".encode())
            time.sleep(5)
            print(port.readline().decode('ascii'))
            os.system("python comp_uit.py")
            print("--------------------------------------------")
        if (x[0] == 2):
            print("Vertuur compartiment: " + comp + " bericht")
            port.write("comp2".encode())
            time.sleep(5)
            print(port.readline().decode('ascii'))
            os.system("python comp_uit.py")
            print("--------------------------------------------")
        if (x[0] == 3):
            print("Vertuur compartiment: " + comp + " bericht")
            port.write("comp3".encode())
            time.sleep(5)
            print(port.readline().decode('ascii'))
            os.system("python comp_uit.py")
            print("--------------------------------------------")
            time.sleep(5)

    mydb.commit()

    # -----------------------------------------------------------------------------------------------------

    mycursor.execute(
        "SELECT comp_kruid FROM compartiment WHERE comp_nummer = '1';")

    for x in mycursor:
        ck1 = str(x[0])
        # print(ck1)

    mydb.commit()

    #  -----------------------------------------------------------------------------------------------------

    mycursor.execute(
        "SELECT comp_kruid FROM compartiment WHERE comp_nummer = '2';")

    for x in mycursor:
        ck2 = str(x[0])
        # print(ck2)

    mydb.commit()

    #  -----------------------------------------------------------------------------------------------------

    mycursor.execute(
        "SELECT comp_kruid FROM compartiment WHERE comp_nummer = '3';")

    for x in mycursor:
        ck3 = str(x[0])
        # print(ck3)

    mydb.commit()

    #  -----------------------------------------------------------------------------------------------------
    mycursor.execute(
        "SELECT hoeveelheid1, hoeveelheid2, hoeveelheid3, kruid1, kruid2, kruid3 FROM mix WHERE maken = 'ja';")

    for x in mycursor:
        value1 = str(x[0])
        value2 = str(x[1])
        value3 = str(x[2])
        kruid1 = str(x[3])
        kruid2 = str(x[4])
        kruid3 = str(x[5])

        if (ck1 == kruid1):
            gram1 = value1
        if (ck1 == kruid2):
            gram1 = value2
        if (ck1 == kruid3):
            gram1 = value3

        if (ck2 == kruid1):
            gram2 = value1
        if (ck2 == kruid2):
            gram2 = value2
        if (ck2 == kruid3):
            gram2 = value3

        if (ck3 == kruid1):
            gram3 = value1
        if (ck3 == kruid2):
            gram3 = value2
        if (ck3 == kruid3):
            gram3 = value3

        grams = gram1 + "," + gram2 + "," + gram3

        print(grams)

        port.write(grams.encode())
        time.sleep(1)
        print(port.readline().decode('ascii'))
        os.system("python mix_uit.py")

    mydb.commit()

    rcv = port2.readline().decode('ascii').strip()

    # volheid = str(rcv)

    if (rcv == 'C1_25'):
        os.system('python comp1_25.py')
        print("compartiment 1 = 25%")

    if (rcv == 'C1_50'):
        os.system('python comp1_50.py')
        print("compartiment 1 = 50%")

    if (rcv == 'C1_100'):
        os.system('python comp1_100.py')
        print("compartiment 1 = 100%")

    if (rcv == 'C2_25'):
        os.system('python comp2_25.py')
        print("compartiment 2 = 25%")

    if (rcv == 'C2_50'):
        os.system('python comp2_50.py')
        print("compartiment 2 = 50%")

    if (rcv == 'C2_100'):
        os.system('python comp2_100.py')
        print("compartiment 2 = 100%")

    if (rcv == 'C3_25'):
        os.system('python comp3_25.py')
        print("compartiment 3 = 25%")

    if (rcv == 'C3_50'):
        os.system('python comp3_50.py')
        print("compartiment 3 = 50%")

    if (rcv == 'C3_100'):
        os.system('python comp3_100.py')
        print("compartiment 3 = 100%")


port.close()
print("script closed")
sys.exit()
