<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Mix;
use App\Kruid;
use App\Compartiment;
use Auth;

class MixController extends Controller
{
    public function show(){
        return view('mix.mix')->with('mix',Mix::all())->with('kruid',Kruid::all());
    }

    public function show_gebruikerMix() {

        if((Auth::user()) == NULL) {
            return view('mix.mix')->with('mix',Mix::all());
        }

        return view('mix.mix')->with('mix', Mix::where('gebruikersnaam','=', Auth::user()->name)->get())
        ->with('gebruiker', auth()->user()->get('name'))->with('kruid',Kruid::all());
    }

    public function create(){
        return view('mix.mix')->with('mix',Mix::all())->with('kruid',Kruid::all());
    }

    public function store(Request $request){

        // $mix_sum3 = $mixHoeveelheid3 . " " . $mixType;

        $mixHoeveelheid1 = $request->input('hoeveelheid1');
        $mixType1 = $request->input("hoeveelheid1_type");
        $mix_sum1 = $mixHoeveelheid1;

        if ($mixType1 == 'tsp') {
            $mix_sum1 = $mixHoeveelheid1 * 5;
        }

        $mixHoeveelheid2 = $request->input('hoeveelheid2');
        $mixType2 = $request->input("hoeveelheid2_type");
        $mix_sum2 = $mixHoeveelheid2;

        if ($mixType2 == 'tsp') {
            $mix_sum2 = $mixHoeveelheid2 * 5;
        }

        $mixHoeveelheid3 = $request->input('hoeveelheid3');
        $mixType3 = $request->input("hoeveelheid3_type");
        $mix_sum3 = $mixHoeveelheid3;

        if ($mixType3 == 'tsp') {
            $mix_sum3 = $mixHoeveelheid3 * 5;
        }

        $mix = new Mix();
        $mix->naam= $request->input('naam');
        $mix->kruid1= $request->input('kruid1');
        $mix->kruid2= $request->input('kruid2');
        $mix->kruid3= $request->input('kruid3');
        $mix->hoeveelheid1= $mix_sum1;
        $mix->hoeveelheid2= $mix_sum2;
        $mix->hoeveelheid3= $mix_sum3;
        $mix->kruid1Type= $mixType1;
        $mix->kruid2Type= $mixType2;
        $mix->kruid3Type= $mixType3;

        if( Auth::user() == null ){
            $mix->gebruikersnaam = 'default';
        }else{
            $mix->gebruikersnaam = Auth::user()->name;
        }

        try{
            $mix->save();
            return redirect('/mix');
            // return $mix;
        }
        catch(\Illuminate\Database\QueryException $e){
            return back()->withError('Make sure you have made a name for the mix. And have atleast 1 spice with the amount.');
        }
    }

    public function edit($mix){

        $mixData = Mix::where('naam','=',$mix)->first();
        $kruid1Value = $mixData->kruid1;
        $kruid2Value = $mixData->kruid2;
        $kruid3Value = $mixData->kruid3;

        $kruid1grams = $mixData->hoeveelheid1;
        $kruid2grams = $mixData->hoeveelheid2;
        $kruid3grams = $mixData->hoeveelheid3;
       
        // $mixHoeveelheidNummer1 = explode(' ', $mixData->hoeveelheid1);
        // $mixHoeveelheidNummer2 = explode(' ', $mixData->hoeveelheid2);
        // $mixHoeveelheidNummer3 = explode(' ', $mixData->hoeveelheid3);

        $mixHoeveelheidNummer1 = $mixData->kruid1Type;
        $mixHoeveelheidNummer2 = $mixData->kruid2Type;
        $mixHoeveelheidNummer3 = $mixData->kruid3Type;

        if ($mixHoeveelheidNummer1 == "gr") {
            $mixType1 = "tsp";
        }
        else if ($mixHoeveelheidNummer1 == "tsp") {
            $kruid1grams = $kruid1grams / 5;
            $mixType1 = "gr";
        }
        if ($mixHoeveelheidNummer2 == "gr") { //$mixHoeveelheidNummer2[1]
            $mixType2 = "tsp";
        }
        else if ($mixHoeveelheidNummer2 == "tsp") {
            $kruid2grams = $kruid2grams / 5;
            $mixType2 = "gr";
        }
        if ($mixHoeveelheidNummer3 == "gr") {
            $mixType3 = "tsp";
        }
        else if ($mixHoeveelheidNummer3 == "tsp") {
            $kruid3grams = $kruid3grams / 5;
            $mixType3 = "gr";
        }


        return view('mix.mixEdit')->with('mix',$mixData)
        ->with('mixHoeveelheidNummer1', $kruid1grams)
        ->with('mixHoeveelheidNummer2', $kruid2grams)
        ->with('mixHoeveelheidNummer3', $kruid3grams)
        ->with('mixHoeveelheidType1', $mixHoeveelheidNummer1)
        ->with('mixHoeveelheidType2', $mixHoeveelheidNummer2)
        ->with('mixHoeveelheidType3', $mixHoeveelheidNummer3)
        ->with('mixType1', $mixType1)
        ->with('mixType2', $mixType2)
        ->with('mixType3', $mixType3)
        ->with('kruid1',Kruid::where('kruid', '!=', $kruid1Value)
        ->where('kruid', '!=', $kruid2Value)
        ->where('kruid', '!=', $kruid3Value)->get());


    }

    public function update(Request $request, $mix){

        $mixHoeveelheid1 = $request->input('hoeveelheid1');
        $mixType1 = $request->input("hoeveelheid1_type");
        $mix_sum1 = $mixHoeveelheid1;

        if ($mixType1 == 'tsp') {
            $mix_sum1 = $mixHoeveelheid1 * 5;
        }

        $mixHoeveelheid2 = $request->input('hoeveelheid2');
        $mixType2 = $request->input("hoeveelheid2_type");
        $mix_sum2 = $mixHoeveelheid2;

        if ($mixType2 == 'tsp') {
            $mix_sum2 = $mixHoeveelheid2 * 5;
        }

        $mixHoeveelheid3 = $request->input('hoeveelheid3');
        $mixType3 = $request->input("hoeveelheid3_type");
        $mix_sum3 = $mixHoeveelheid3;

        if ($mixType3 == 'tsp') {
            $mix_sum3 = $mixHoeveelheid3 * 5;
        }

        $mix = Mix::find($mix);
        $mix->naam= $request->input('naam');
        $mix->kruid1= $request->input('kruid1');
        $mix->kruid2= $request->input('kruid2');
        $mix->kruid3= $request->input('kruid3');
        $mix->hoeveelheid1= $mix_sum1;
        $mix->hoeveelheid2= $mix_sum2;
        $mix->hoeveelheid3= $mix_sum3;
        $mix->kruid1Type= $mixType1;
        $mix->kruid2Type= $mixType2;
        $mix->kruid3Type= $mixType3;

        if (Auth::user() == null) {
            $mix->gebruikersnaam = 'default';
        } else {
            $mix->gebruikersnaam = Auth::user()->name;
        }

        try {
            $mix->save();
            return redirect('/mix');
            // return $mix;
        }
        catch(\Illuminate\Database\QueryException $e) {
            return back()->withError('Make sure you have made a name for the mix. And have atleast 1 spice with the amount.');
        }

    }

    public function destroy($mix){
        $mix = Mix::find($mix);
        try{
            $mix->delete();
            return redirect('/mix');
        }
        catch(Exception $e){
            return redirect('/kruid');
        }
    }

    // public function uitvoeren($mix) {
    //     try{
    //       Mix::where('naam', $mix)->update([
    //         'maken' => 'ja'
    //       ]);
    //       return redirect('/mix');
    //     }
    //     catch(Exception $e){
    //         return redirect('/mix');
    //     }
    // }


    public function uitvoeren($mix) {

        $kruid1 = DB::table('mix')->where('naam', '=', $mix)->value('kruid1');
        $kruid2 = DB::table('mix')->where('naam', '=', $mix)->value('kruid2');
        $kruid3 = DB::table('mix')->where('naam', '=', $mix)->value('kruid3');

        $complist = DB::table('compartiment')->pluck('comp_kruid')->toArray();

        if (in_array($kruid1, $complist) && in_array($kruid2,$complist) && in_array($kruid3,$complist)) {

            $stringRoute = "/mix/update/".$mix;

            try {
            Mix::where('naam', $mix)->update([
                'maken' => 'ja'
            ]);
            
            return redirect('/mix');
            }

            catch(Exception $e) {
                return back()->withError('There is one or more spices in this mix that is not in a compartiment. Choose a different mix or update the compartiments.');
            }
        }
        else {
            return back()->withError('There is one or more spices in this mix that is not in a compartiment. Choose a different mix or update the compartiments.');
        }
    }



    public function checkgoed($mix) {
        return view('mix.mixUitvoeren')->with(Mix::all());
    }

    // public function uitvoeren($mix) {
    //   $mix = Mix::where('naam', '=',$mix)->first();

    //   if ($mix->maken == 'nee') {
    //     $mix->maken = 'ja';
    //   }

    //   else {
    //     $mix->maken = 'nee';
    //   }
    //   $mix->save();
    //   return redirect('/mix');
    // }

    public function check($mix) {
        // if ($mix->kruid1 == Compartiment::get('comp_kruid')) {
        //     return redirect('/mix/update/' + $mix);
        // }
        $kruid1 = DB::table('mix')->where('naam', '=', $mix)->value('kruid1');
        $kruid2 = DB::table('mix')->where('naam', '=', $mix)->value('kruid2');
        $kruid3 = DB::table('mix')->where('naam', '=', $mix)->value('kruid3');
        /* $kruid1 = Mix::get('kruid1');

        $kruid2 = Mix::get('kruid2');
        $kruid3 = Mix::get('kruid3'); */
        $complist = DB::table('compartiment')->pluck('comp_kruid')->toArray();
        if (in_array($kruid1,$complist) && in_array($kruid2,$complist) && in_array($kruid3,$complist)){
            $stringRoute = "/mix/update/".$mix;
            try {
                return redirect('');
            } catch (\Throwable $th) {
                return redirect('/error');
            }

        }
        else {
            return redirect('/error');
        }
    }

}
