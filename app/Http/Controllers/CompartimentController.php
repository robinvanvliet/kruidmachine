<?php

namespace App\Http\Controllers;
use DB;
use App\Compartiment;
use App\Kruid;
use Illuminate\Http\Request;
use Auth;

class CompartimentController extends Controller
{
  public function show() {
    if((Auth::user()) == NULL){
      return view('welcome')->with('kruid', Kruid::get('kruid'))
      ->with('comp_1', Compartiment::all()->where('comp_nummer', 1))
      ->with('comp_2', Compartiment::all()->where('comp_nummer', 2))
      ->with('comp_3', Compartiment::all()->where('comp_nummer', 3));
      #dit fixen
    }
    return view('welcome2')->with('kruid', Kruid::get('kruid'))
    ->with('comp_1', Compartiment::all()->where('comp_nummer', 1))
    ->with('comp_2', Compartiment::all()->where('comp_nummer', 2))
    ->with('comp_3', Compartiment::all()->where('comp_nummer', 3));
    #dit fixen
  }

  public function comp1_add_page($compartiment) {

    $compNummer = $compartiment;

    return view('kruid.kruid_add_comp1')
    ->with('kruid', Kruid::all()
    ->where('comp_nummer', '==', Null))
    ->with('compartiment', $compNummer);

    return $compNummer;
}  

  public function add_comp1(Request $request, $compartiment) {
    $kruidNaam = $request->input('kruid');

    $updatedComp_kruid = Kruid::where('kruid', $kruidNaam)->first();
    $updatedComp_kruid->comp_nummer = $compartiment;

    $compartiment = new Compartiment();
    $compartiment->comp_nummer= $updatedComp_kruid->comp_nummer;
    $compartiment->comp_kruid= $updatedComp_kruid->kruid;
    $compartiment->comp_veranderen= "ja";
           
    try{
      $compartiment->save();
      $updatedComp_kruid->save();

      return redirect('/'); //kruid/kruid_update/'.$compartiment
    }
    catch(Exception $e){
      return redirect('/home');
    }
  }

  // public function update_first(Request $request, $compartiment) {

  //   $updatedComp_kruid = Kruid::where('kruid', $kruidNaam)->first();
  //   $updatedComp_kruid->comp_nummer = $compartiment;

  //   $compartimentCheck = Compartiment::all()->where('comp_nummer',  $compartiment)->first();

  //   try {
  //     $updatedComp_kruid->save();

      // Compartiment::where('comp_nummer', $compartiment)->update([ //comp_veranderen naar "ja" zetten
      //   'comp_veranderen' => 'ja'
      // ]);

  //     return redirect('/');
  //   }
  //   catch(Exception $e) {
  //     return redirect('/home');
  //   }
  // }

  public function comp1($compartiment) {
      $compData = Compartiment::where('comp_nummer','=',$compartiment)->first();
      return view('kruid.kruid_update_comp1')
      ->with('kruid', Kruid::all()
      ->where('comp_nummer', '==', Null))
      ->with('compartiment', $compData);
  }

  public function update_comp1(Request $request, $compartiment) {
      $kruidNaam = $request->input('kruid');

      $updatedOud_kruid = Kruid::where('comp_nummer', $compartiment)->first();
      $updatedOud_kruid->comp_nummer = Null;

      $updatedComp_kruid = Kruid::where('kruid', $kruidNaam)->first();
      $updatedComp_kruid->comp_nummer = $compartiment;

      $compartiment = Compartiment::where('comp_nummer', $compartiment)->first();
      $compartiment->comp_kruid = $kruidNaam;

      $compartiment->comp_veranderen= "ja";
      
      try{
        $updatedComp_kruid->save();
        $updatedOud_kruid->save();
        $compartiment->save();
        return redirect('/');
    }
    catch(Exception $e){
        return redirect('/home');
    }
  }
    // public function show() {
    //   return view('kruid.kruidComp')->with('kruid', Kruid::all())
    //   ->with('comp_1', Compartiment::all()->where('comp_nummer', 1))
    //   ->with('comp_2', Compartiment::all()->where('comp_nummer', 2))
    //   ->with('comp_3', Compartiment::all()->where('comp_nummer', 3));
    //   #dit fixen

    // }
}
